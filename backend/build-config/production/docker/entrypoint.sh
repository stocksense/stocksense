#!/bin/sh

echo "starting backend"

while ! nc -z $PCFG_DB_HOST $PCFG_DB_PORT; do
    echo "Waiting for PostgreSQL..."
    sleep 1
done

echo "PostgreSQL started"

python manage.py migrate

DJANGO_SUPERUSER_USERNAME=$PCFG_ADMIN_USERNAME \
DJANGO_SUPERUSER_PASSWORD=$PCFG_ADMIN_PASSWORD \
python manage.py createsuperuser --noinput --email admin@stocksense.ai

supervisord --nodaemon --configuration /usr/local/etc/supervisord.conf
