from django.urls import include, path
from rest_framework_simplejwt import views as jwt_views

auth_patterns = [
    path("login", jwt_views.TokenObtainPairView.as_view(), name="login-obtain-token"),
    path(
        "login/refresh",
        jwt_views.TokenRefreshView.as_view(),
        name="login-token-refresh",
    ),
]

urlpatterns = [path("auth/", include(auth_patterns))]
