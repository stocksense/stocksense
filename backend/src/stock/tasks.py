from .scraper import StockScraper
from .eligibility_calculator import StockEligibilityUpdater


def scrape_stocks():
    scraper = StockScraper()
    scraper.execute()


def update_stocks_eligibility():
    updater = StockEligibilityUpdater()
    updater.execute()
