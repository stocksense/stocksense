from django.utils.timezone import make_aware
from datetime import datetime
from django.conf import settings
from django.db.models import Prefetch, OuterRef, Subquery, Min, Max
from .models import Stock, StockPrice, StockSentiment


# Updates each stock's eligibility score based on sentiment and price change
class StockEligibilityUpdater:
    def execute(self):

        config = settings.STOCK_ELIGIBILITY

        price_weight = config["price_weight"]
        sentiment_weight = config["sentiment_weight"]
        delta = config["recent_period"]

        # calculate the changes
        calculator = StockChangeCalculator()
        since = datetime.now() - delta
        calculated = calculator.calculate(since)

        for item in calculated:
            id = item["id"]
            price_change = item["price"]["change"]
            sentiment_change = item["sentiment"]["change"]
            eligibility = (
                abs(price_change) * price_weight
                + abs(sentiment_change) * sentiment_weight
            )

            stock = Stock.objects.get(id=id)
            stock.eligibility = eligibility
            stock.save()


class StockChangeCalculator:
    def calculate(self, since):

        since = make_aware(since)

        price_id_subquery = Subquery(
            StockPrice.objects.filter(stock_id=OuterRef("stock_id"))
            .order_by("-timestamp")
            .values_list("id", flat=True)[:1]
        )

        sentiment_id_subquery = Subquery(
            StockSentiment.objects.filter(stock_id=OuterRef("stock_id"))
            .order_by("-timestamp")
            .values_list("id", flat=True)[:1]
        )

        stocks = Stock.objects.prefetch_related(
            Prefetch(
                "stockprice_set",
                queryset=StockPrice.objects.filter(id__in=price_id_subquery),
                to_attr="latest_price",
            ),
            Prefetch(
                "stocksentiment_set",
                queryset=StockSentiment.objects.filter(id__in=sentiment_id_subquery),
                to_attr="latest_sentiment",
            ),
        )

        price_min_max_per_stock = (
            StockPrice.objects.filter(timestamp__gte=since)
            .values("stock_id")
            .annotate(min_price=Min("price"), max_price=Max("price"))
        )

        sentiment_min_max_per_stock = (
            StockSentiment.objects.filter(timestamp__gte=since)
            .values("stock_id")
            .annotate(min_sentiment=Min("sentiment"), max_sentiment=Max("sentiment"))
        )

        result = []

        for stock in stocks:

            price_change = 0
            price_min = None
            price_max = None
            price_min_max = [
                x for x in price_min_max_per_stock if x["stock_id"] == stock.id
            ]
            if len(price_min_max) > 0:

                latest_price = stock.latest_price[0].price
                stock_min_max = price_min_max[0]
                price_min = stock_min_max["min_price"]
                price_max = stock_min_max["max_price"]

                if abs(price_min - latest_price) > abs(price_max - latest_price):
                    price_change = self.calculate_percent_change(
                        price_min, latest_price
                    )
                else:
                    price_change = self.calculate_percent_change(
                        price_max, latest_price
                    )

            sentiment_change = 0
            sentiment_min = None
            sentiment_max = None
            sentiment_min_max = [
                x for x in sentiment_min_max_per_stock if x["stock_id"] == stock.id
            ]
            if len(sentiment_min_max) > 0:

                latest_sentiment = stock.latest_sentiment[0].sentiment
                stock_min_max = sentiment_min_max[0]
                sentiment_min = stock_min_max["min_sentiment"]
                sentiment_max = stock_min_max["max_sentiment"]

                if abs(sentiment_min - latest_sentiment) > abs(
                    sentiment_max - latest_sentiment
                ):
                    sentiment_change = self.calculate_sentiment_change(
                        sentiment_min, latest_sentiment
                    )
                else:
                    sentiment_change = self.calculate_sentiment_change(
                        sentiment_max, latest_sentiment
                    )

            result.append(
                {
                    "id": stock.id,
                    "price": {
                        "min": price_min,
                        "max": price_max,
                        "change": price_change,
                    },
                    "sentiment": {
                        "min": sentiment_min,
                        "max": sentiment_max,
                        "change": sentiment_change,
                    },
                }
            )
        return result

    def calculate_sentiment_change(self, original, new):

        return (new - original) / 2  # -1.0 - 1.0 to 0.0 - 1.0

    def calculate_percent_change(self, original, new):

        return (new - original) / original
