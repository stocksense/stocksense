from django.contrib import admin
from .models import Stock, StockPrice, StockSentiment


class StockPriceInline(admin.TabularInline):
    model = StockPrice


@admin.register(Stock)
class StockAdmin(admin.ModelAdmin):

    inlines = [StockPriceInline]


admin.site.register(StockPrice)
admin.site.register(StockSentiment)
