from django.db import migrations
from django_q.models import Schedule


def create_schedules(apps, schema_editor):
    Schedule.objects.create(
        func="stock.tasks.update_stocks_eligibility",
        schedule_type=Schedule.MINUTES,
        minutes=10,
        repeats=-1,
    )


class Migration(migrations.Migration):

    dependencies = [
        ("stock", "0012_stock_eligibility"),
    ]

    operations = [
        migrations.RunPython(create_schedules),
    ]
