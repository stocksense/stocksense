from django.contrib import admin
from .models import PostProvider, PostComment, Post

admin.site.register(PostProvider)
admin.site.register(PostComment)


class PostCommentInline(admin.TabularInline):
    model = PostComment


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):

    inlines = [PostCommentInline]
