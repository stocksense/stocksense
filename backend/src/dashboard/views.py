from django.db.models import Prefetch, Q, OuterRef, Subquery
from django.contrib.auth import get_user_model
from rest_framework.views import APIView
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from post.models import Post, PostComment
from stock.models import Stock, StockPrice, StockSentiment
from trader.models import Trader


class StockDetailsFetcher:
    """
    Helper class used by the views to build stock item with trader details
    """

    def build_stock_item(self, stock):
        is_starred = hasattr(stock, "starred") and len(stock.starred) > 0

        price_object = stock.latest_price[0] if len(stock.latest_price) > 0 else None
        sentiment_object = (
            stock.latest_sentiment[0] if len(stock.latest_sentiment) > 0 else None
        )

        price = 0.0
        price_timestamp = ""
        if price_object is not None:
            price = price_object.price
            price_timestamp = price_object.timestamp

        sentiment = 0.0
        sentiment_timestamp = ""
        if sentiment_object is not None:
            sentiment = sentiment_object.sentiment
            sentiment_timestamp = sentiment_object.timestamp

        item = {
            "id": stock.id,
            "symbol": stock.symbol,
            "name": stock.title,
            "price": price,
            "price_timestamp": price_timestamp,
            "sentiment": sentiment,
            "sentiment_timestamp": sentiment_timestamp,
            "is_starred": is_starred,
            "eligibility": stock.eligibility,
        }
        return item

    def prefetch_starred(self, request, stock_query):
        user = request.user
        if user is not None and type(user) == get_user_model():
            stock_query = stock_query.prefetch_related(
                Prefetch(
                    "traders_starred",
                    queryset=Trader.objects.filter(id=user.trader.id),
                    to_attr="starred",
                )
            )
        return stock_query

    def prefetch_related_prices(self, stocks_query):
        price_id_subquery = Subquery(
            StockPrice.objects.filter(stock_id=OuterRef("stock_id"))
            .order_by("-timestamp")
            .values_list("id", flat=True)[:1]
        )

        stocks_query = stocks_query.prefetch_related(
            Prefetch(
                "stockprice_set",
                queryset=StockPrice.objects.filter(id__in=price_id_subquery),
                to_attr="latest_price",
            )
        )

        return stocks_query

    def prefetch_related_sentiments(self, stocks_query):
        sentiment_id_subquery = Subquery(
            StockSentiment.objects.filter(stock_id=OuterRef("stock_id"))
            .order_by("-timestamp")
            .values_list("id", flat=True)[:1]
        )

        stocks_query = stocks_query.prefetch_related(
            Prefetch(
                "stocksentiment_set",
                queryset=StockSentiment.objects.filter(id__in=sentiment_id_subquery),
                to_attr="latest_sentiment",
            )
        )

        return stocks_query


class TraderStockDetails(APIView):
    """
    Returns stock details by symbol with trader specific information, e.g. starred
    """

    permission_classes = (AllowAny,)

    stock_details_fetcher = StockDetailsFetcher()

    def get(self, request, symbol=""):

        stock_query = Stock.objects.filter(symbol=symbol)
        stock_query = self.stock_details_fetcher.prefetch_starred(request, stock_query)
        stock_query = self.stock_details_fetcher.prefetch_related_prices(stock_query)
        stock_query = self.stock_details_fetcher.prefetch_related_sentiments(
            stock_query
        )

        stock = stock_query.first()
        if stock is None:
            return Response(False)

        item = self.stock_details_fetcher.build_stock_item(stock)
        return Response(item)


class ListTraderStocksView(APIView):
    """
    Lists stocks with trader specific information, e.g. starred
    """

    permission_classes = (AllowAny,)

    stock_details_fetcher = StockDetailsFetcher()

    def get(self, request, format=None):

        params = self.parse_params(request)

        stocks_query = Stock.objects.order_by("title")
        stocks_query = self.query_filter(stocks_query, params["filter"])

        list_type = params["type"]
        order = params["order"]

        # prefetch related user starrings
        stocks_query = self.stock_details_fetcher.prefetch_starred(
            request, stocks_query
        )

        # prefetch related prices and sentiments
        stocks_query = self.stock_details_fetcher.prefetch_related_prices(stocks_query)
        stocks_query = self.stock_details_fetcher.prefetch_related_sentiments(
            stocks_query
        )

        if order == "promising":
            stocks_query = stocks_query.order_by("-eligibility")
        else:
            stocks_query = stocks_query.order_by("title")

        stocks = stocks_query.all()
        items = []

        for stock in stocks:

            item = self.stock_details_fetcher.build_stock_item(stock)

            if list_type == "starred" and not item["is_starred"]:
                continue

            items.append(item)

        if order == "starred":
            # TODO: try find solutions to sort the prefetched query in SQL
            items.sort(key=lambda x: x["is_starred"], reverse=True)

        return Response(items)

    def query_filter(self, query, filter=None):
        if filter is not None:
            query = query.filter(
                Q(title__icontains=filter) | Q(symbol__icontains=filter)
            )

        return query

    def parse_params(self, request):
        params_data = {
            "filter": request.query_params.get("filter", None),
            "type": request.query_params.get("type", "all"),
            "order": request.query_params.get("order", "name"),
        }

        params_serializer = self.StockListParamsSerializer(data=params_data)
        if not params_serializer.is_valid():
            raise ValueError("Invalid input parameters", params_serializer.errors)

        params = params_serializer.validated_data
        return params

    class StockListParamsSerializer(serializers.Serializer):

        LIST_TYPE = (("all", "All"), ("starred", "Starred"))
        ORDER = (
            ("name", "by name"),
            ("starred", "by starred"),
            ("promising", "by promising"),
        )

        filter = serializers.CharField(required=False, default=None, allow_null=True)
        type = serializers.ChoiceField(
            choices=LIST_TYPE, required=False, default="all", allow_null=True
        )
        order = serializers.ChoiceField(
            choices=ORDER, required=False, default="anme", allow_null=True
        )


class QueryStockGraphView(APIView):
    """
    View handler for stock graphs
    """

    permission_classes = (AllowAny,)

    def get(self, request, format=None, id=0):

        params = self.parse_params(request)

        prices = self.query_prices(id, from_ts=params["from_ts"], to_ts=params["to_ts"])
        sentiments = self.query_sentiments(
            id, from_ts=params["from_ts"], to_ts=params["to_ts"]
        )

        serialized_prices = [self.StockPriceSerializer(item).data for item in prices]
        serialized_sentiments = [
            self.StockSentimentSerializer(item).data for item in sentiments
        ]

        return Response(
            {"prices": serialized_prices, "sentiments": serialized_sentiments}
        )

    def query_prices(self, stock_id, from_ts=None, to_ts=None):
        price_query = StockPrice.objects.filter(stock_id=stock_id).order_by("timestamp")
        price_query = self.query_timestamp_filter(
            price_query, from_ts=from_ts, to_ts=to_ts
        )

        return price_query

    def query_sentiments(self, stock_id, from_ts=None, to_ts=None):
        sentiment_query = StockSentiment.objects.filter(stock_id=stock_id).order_by(
            "timestamp"
        )
        sentiment_query = self.query_timestamp_filter(
            sentiment_query, from_ts=from_ts, to_ts=to_ts
        )

        return sentiment_query

    def query_timestamp_filter(self, query, from_ts=None, to_ts=None):
        if from_ts is not None:
            query = query.filter(timestamp__gte=from_ts)
        if to_ts is not None:
            query = query.filter(timestamp__lte=to_ts)

        return query

    def parse_params(self, request):
        params_data = {
            "from_ts": request.query_params.get("from", None),
            "to_ts": request.query_params.get("to", None),
        }

        params_serializer = self.StockGraphParamsSerializer(data=params_data)
        if not params_serializer.is_valid():
            raise ValueError("Invalid input parameters", params_serializer.errors)

        params = params_serializer.validated_data
        return params

    class StockPriceSerializer(serializers.Serializer):
        timestamp = serializers.DateTimeField(read_only=True)
        price = serializers.FloatField(read_only=True)

    class StockSentimentSerializer(serializers.Serializer):
        timestamp = serializers.DateTimeField(read_only=True)
        sentiment = serializers.FloatField(read_only=True)

    class StockGraphParamsSerializer(serializers.Serializer):
        from_ts = serializers.DateTimeField(
            required=False, default=None, allow_null=True
        )
        to_ts = serializers.DateTimeField(required=False, default=None, allow_null=True)


class TraderStockPosts(APIView):
    """
    Returns stock details by symbol with trader specific information, e.g. starred
    """

    permission_classes = (AllowAny,)

    stock_details_fetcher = StockDetailsFetcher()

    def get(self, request, symbol=""):

        POST_COUNT = 5
        COMMENT_COUNT = 5

        # prefetch last X comments
        comment_id_subquery = Subquery(
            PostComment.objects.filter(post_id=OuterRef("post_id"))
            .order_by("-timestamp")
            .values_list("id", flat=True)[:COMMENT_COUNT]
        )
        posts_query = Post.objects.filter(stock=symbol).order_by("?")  # random posts
        posts_query = posts_query.prefetch_related(
            Prefetch(
                "comments",
                queryset=PostComment.objects.filter(id__in=comment_id_subquery),
                to_attr="sample_comments",
            )
        )

        # fetch last Y posts randomly
        posts_query = posts_query[:POST_COUNT]

        def map_comment(comment):
            return {
                "id": comment.id,
                "timestamp": comment.timestamp,
                "title": comment.title,
                "body": comment.body,
            }

        def map_post(post):
            comments = list(map(map_comment, post.sample_comments))

            return {
                "id": post.id,
                "timestamp": post.timestamp,
                "title": post.title,
                "body": post.body,
                "provider": post.provider_id,
                "comments": comments,
            }

        result = list(map(map_post, posts_query))

        return Response(result)
