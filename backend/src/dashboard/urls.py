from django.urls import path
from .views import (
    ListTraderStocksView,
    QueryStockGraphView,
    TraderStockDetails,
    TraderStockPosts,
)

urlpatterns = [
    path("dashboard/stocks", ListTraderStocksView.as_view(), name="list-trader-stocks"),
    path(
        "dashboard/stocks/<int:id>/graph",
        QueryStockGraphView.as_view(),
        name="trader-stock-graph",
    ),
    path(
        "dashboard/stocks/<symbol>",
        TraderStockDetails.as_view(),
        name="trader-stock-details",
    ),
    path(
        "dashboard/stocks/<symbol>/posts",
        TraderStockPosts.as_view(),
        name="trader-stock-posts",
    ),
]
