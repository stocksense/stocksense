from django.db import models


class RedditScraperState(models.Model):

    last_date = models.DateTimeField(
        help_text="The timestamp of the lastly downloaded reddit post"
    )
