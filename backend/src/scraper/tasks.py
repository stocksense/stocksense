from .reddit.scraper import RedditScraper
from .twitter.scraper import TwitterScraper
import datetime
from django.utils import timezone


def scrape_reddit():
    to_date = timezone.now()
    from_date = to_date - datetime.timedelta(days=3)
    scraper = RedditScraper()
    scraper.execute(from_date, to_date)


def scrape_twitter():
    scraper = TwitterScraper()
    scraper.execute()
