from datetime import datetime as dt
from django.utils import timezone
from django.utils.timezone import make_aware
from .models import TwitterScraperState
from post.models import Post
import datetime
import pytz
import tweepy
import pandas as pd

utc = pytz.UTC

CONSUMER_KEY = "XUo4KXp1UqoOvLZ0b19oUHuUR"
CONSUMER_SECRET = "XfwmaRhwH729toSiHuAdtHSiUwWzlbmLoz61eXcKpNg0OiD562"
ACCESS_TOKEN = "1455537243989450762-z11DYKcUagQYYqDej1xJGHUSU4MAlS"
ACCESS_TOKEN_SECRET = "13Gf7sE8OWaZz8fcyl7GG28dPRJF80eEFR6RJjvEFTUXV"

stocks = [
    "Apple",
    "NVIDIA",
    "Tesla",
    "AMD",
    "Amazon",
    "Coinbase",
    "Microsoft",
    "Facebook",
    "Upstart",
    "Roblox",
    "Google",
    "Lucid Motors",
    "Gamestop",
    "AMC",
    "SmileDirectClub",
    "Rocket Lab",
    "Blackberry",
    "Beyond Meat",
]
stocks_abbr = {
    "Apple": "AAPL",
    "NVIDIA": "NVDA",
    "Tesla": "TSLA",
    "AMD": "AMD",
    "Amazon": "AMZN",
    "Coinbase": "COIN",
    "Microsoft": "MSFT",
    "Facebook": "FB",
    "Upstart": "UPST",
    "Roblox": "RBLX",
    "Google": "GOOGL",
    "Lucid Motors": "LCID",
    "Gamestop": "GME",
    "AMC": "AMC",
    "SmileDirectClub": "SDC",
    "Rocket Lab": "RKLB",
    "Blackberry": "BB",
    "Beyond Meat": "BYND",
}


class TwitterScraper:
    def __init__(self):
        self.auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        self.auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        self.api = tweepy.API(self.auth)  # tweepy.api.API
        self.stocks = stocks  # Python list of stocks that we want to grab tweets for
        self.num_of_tweets = 15  # Number of the most recent tweets we want to collect
        try:
            self.api.verify_credentials()
            print("Authentication OK")
        except Exception as e:
            print("Error during authentication")
            print(e)

    def tweet_collector(self):
        """
        Returns: a list with dimensions (num_of_tweets*len(stocks), 3) that saves
        the stock, timestamp and tweet IF the tweet is in English language.
        A stream gives a random selection (1% of tweets). We decided to collect a mix of the most popular tweets
        and recent ones, as this is more representative of general sentiment than a random selection
        in our opinion.
        """
        tweets_list = []
        stocks = [
            stock + " -filter:retweets" for stock in self.stocks
        ]  # Filter out retweets
        for stock in stocks:
            print(stock)
            tweets = self.api.search_tweets(
                q=str(stock), count=self.num_of_tweets, lang="en", result_type="mixed"
            )
            for tweet in tweets:
                tweets_list.append(
                    [tweet.id, stock[:-16], tweet.created_at, tweet.text]
                )
        return tweets_list

    def execute(self):
        now = dt.now(tz=timezone.utc)
        state = TwitterScraperState.objects.first()
        if state:
            state.last_date = now
            state.save()
        else:
            state = TwitterScraperState(last_date=now)
            state.save()

        tweets_data = self.tweet_collector()
        df = pd.DataFrame(tweets_data, columns=["id", "stock", "timestamp", "text"])
        for index, row in df.iterrows():
            try:
                post, created = Post.objects.update_or_create(
                    provider_id="twitter",
                    source_id=str(row["id"]),
                )
                post.stock = (stocks_abbr[row["stock"].strip()],)
                post.timestamp = (
                    make_aware(
                        datetime.datetime.fromtimestamp(row["timestamp"].timestamp())
                    ),
                )
                post.body = (row["text"],)
                post.save()
            except Exception as e:
                print(e)
