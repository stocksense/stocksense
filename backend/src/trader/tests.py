from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from user.tests import AuthenticatedTestCaseBase
from stock.models import Stock
from model_bakery import baker


class TraderAPITests(APITestCase):
    def test_register_trader(self):
        url = reverse("register-trader")
        data = {
            "email": "trader@stocksense.ai",
            "password": "Pass$ord123",
            "password2": "Pass$ord123",
            "first_name": "Firstname",
            "last_name": "Lastname",
            "trader": {"plan": "standard"},
        }

        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        user = get_user_model().objects.get(email="trader@stocksense.ai")
        self.assertEquals("trader@stocksense.ai", user.email)
        self.assertEquals("Firstname", user.first_name)
        self.assertEquals("Lastname", user.last_name)
        self.assertEquals("standard", user.trader.plan)


class DashboardStarUnstarTests(AuthenticatedTestCaseBase):
    def test_star_stock(self):
        # given
        stock = baker.make(Stock)

        data = {"starred": True}
        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})

        # when
        response = self.client.put(url, data, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, True)

    def test_star_again_idempotent(self):
        # given
        stock = baker.make(Stock)

        data = {"starred": True}
        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})

        # when
        response = self.client.put(url, data, format="json")
        response = self.client.put(url, data, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, True)

    def test_unstar(self):
        # given
        stock = baker.make(Stock)

        data = {"starred": True}
        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})
        self.client.put(url, data, format="json")

        # when
        data = {"starred": False}
        response = self.client.put(url, data, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, False)

    def test_starred_is_false_when_not_starred(self):

        # given
        stock = baker.make(Stock)
        url = reverse("trader-star-unstar-stock", kwargs={"id": stock.id})

        # when
        response = self.client.get(url, format="json")

        # then
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, False)
