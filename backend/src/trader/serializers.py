from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import get_user_model
from rest_framework.validators import UniqueValidator
from rest_framework import serializers
from .models import Trader


class TraderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trader
        fields = ["plan"]

    def validate(self, attrs):
        if attrs["plan"] == Trader.Plan.NONE:
            raise serializers.ValidationError({"plan": "Plan shouldn't be None"})
        return attrs


class RegisterTraderSerializer(serializers.ModelSerializer):

    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=get_user_model().objects.all())],
    )

    password = serializers.CharField(
        write_only=True,
        required=True,
        validators=[validate_password],
        style={"input_type": "password", "placeholder": "Password"},
    )
    password2 = serializers.CharField(
        write_only=True,
        required=True,
        style={"input_type": "password", "placeholder": "Password"},
    )
    first_name = serializers.CharField(max_length=150, required=True)
    last_name = serializers.CharField(max_length=150, required=True)

    trader = TraderSerializer()

    class Meta:
        model = get_user_model()
        fields = ["email", "password", "password2", "first_name", "last_name", "trader"]
        extra_kwargs = {
            "first_name": {"required": True},
            "last_name": {"required": True},
        }

    def validate(self, attrs):
        if attrs["password"] != attrs["password2"]:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match"}
            )

        return attrs

    def create(self, validated_data):

        User = get_user_model()
        user = User.objects.create_user(
            email=validated_data["email"], password=validated_data["password"]
        )
        user.first_name = validated_data["first_name"]
        user.last_name = validated_data["last_name"]

        # create the associated trader model
        trader_data = validated_data.pop("trader")
        if user.trader is None:
            Trader.objects.create(user=user, plan=trader_data["plan"])
        else:
            user.trader.plan = trader_data["plan"]
            user.trader.save()
        user.save()

        return user
