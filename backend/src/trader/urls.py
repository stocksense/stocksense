from django.urls import path
from .views import RegisterTraderView, StarUnstarTraderStockView

urlpatterns = [
    path("trader/register", RegisterTraderView.as_view(), name="register-trader"),
    path(
        "trader/stocks/<int:id>/starred",
        StarUnstarTraderStockView.as_view(),
        name="trader-star-unstar-stock",
    ),
]
