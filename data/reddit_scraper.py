import praw
import datetime as dt
import pandas as pd
from praw.models import MoreComments
import json


f = open("config.json")

config = json.load(f)


CLIENT_ID = config['creditentials']['client_id']
SECRET_KEY = config['creditentials']['secret_key']
APP_NAME= config['creditentials']['app_name']
USERNAME = config['creditentials']['username']
PASSWORD = config['creditentials']['password']

reddit = praw.Reddit(
                        client_id=CLIENT_ID,
                        client_secret=SECRET_KEY,
                        user_agent=APP_NAME,
                        username=USERNAME,
                        password=PASSWORD
                    )



def get_date(submission):
	time = submission.created
	return dt.datetime.fromtimestamp(time)


def load_recent_data():
    return pd.read_pickle('scraped_data.pkl.zip',compression='zip')
    #currently to pickle, TO DO: connect to DB and load the data

def save_current_data(df):
    df.to_pickle('scraped_data.pkl.zip',compression='zip')
    #currently to pickle, TO DO: connect to DB and save the data

def download_data():
    old_data = load_recent_data()
    last_date = old_data.iloc[-1]['date']

    new_data = []

    for stock in config['scraper_config']['stocks']:
        keywords = stock['keywords']
        keywords.extend(config['scraper_config']['common_keywords'])
        subreddits = stock['subreddits']
        subreddits.extend(config['scraper_config']['common_subreddits'])
        for sub in subreddits:
            subreddit = reddit.subreddit(sub)
            for keyword in keywords:
                for submission in subreddit.search(keyword, sort = 'new'):

                    current_date = pd.Timestamp(get_date(submission))

                    if(last_date>=current_date):
                        break

                    tmp = {'id':submission.id,
                           'date': get_date(submission),
                           'title':submission.title,
                           'author':submission.author,
                           'content':submission.selftext.strip('\n')}

                    comments = ''
                    submission.comment_sort = 'best'
                    i  = 0
                    for top_level_comment in submission.comments:
                        if isinstance(top_level_comment, MoreComments):
                            continue
                        if (not top_level_comment.stickied and str(top_level_comment.body) != '[deleted]'): #Filter deleted
                            comments += str(top_level_comment.body).strip() + "|"
                            i += 1
                        if i == 10:
                            break
                    tmp['comments'] = comments
                    if not tmp['content'] == '': #Filter pictures
                        if(tmp['id'] not in [x['id'] for x in new_data]):
                            new_data.append(tmp)

    df = pd.DataFrame(new_data)
    df1 = pd.concat([old_data,df],join='outer')
    df2 = df1.sort_values(by='date',axis = 0,ignore_index=True).copy()
    save_current_data(df2)
    print(df2)

download_data()