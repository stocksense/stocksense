import time
import json

from alpha_vantage.timeseries import TimeSeries


f = open("config.json")
config = json.load(f)

class StockPriceLoader():

    def __init__(self):
        self.key = "30G5375G2J2RO3CH"
        self.stocks = []
        self.time_series = TimeSeries(key = self.key, output_format = 'pandas')

    def execute(self):
        i = 0
        for stock in config['scraper_config']['stocks']:
            try:
                tmp = self.time_series.get_intraday(stock['name'], outputsize='compact')
            except:
                time.sleep(60)
                tmp = self.time_series.get_intraday(stock['name'], outputsize='compact')
            self.stocks.append({'name': stock['name'], 'meta': tmp[1], 'data': tmp[0]})
            i += 1
            if i % 5 == 0:
                time.sleep(60)

        return self.stocks

if __name__ == '__main__':
    stock_loader = StockPriceLoader()
    stocks = stock_loader.execute()
    print(len(stocks))
    print(stocks)
