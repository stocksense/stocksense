import nltk

nltk.download("vader_lexicon")
from nltk.sentiment.vader import SentimentIntensityAnalyzer

sid = SentimentIntensityAnalyzer()


def sentiment_analysis(list_of_text, mean=True):
    """
    Function to calculate the sentiment of a text using Vader
    :param list_of_text: A list or series of Texts (strings)
    :param mean: Boolean whether to return the mean of all tweets or single compound scores
    :return: One or multiple compound scores ranging between -1 and 1, where -1 is negative and +1 is positive sentiment
    """
    scores = list_of_text.apply(lambda text: sid.polarity_scores(text))
    compound = scores.apply(lambda score_dict: score_dict["compound"])
    if mean:
        return compound.mean()
    else:
        return compound
