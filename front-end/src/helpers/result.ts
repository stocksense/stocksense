export type Ok<TValue> = { success: true; value: TValue };
export type Error<TError> = { success: false; error: TError };
export type Result<TValue, TError> = Ok<TValue> | Error<TError>;

export function ok<TValue>(value: TValue): Ok<TValue> {
	return { success: true, value };
}

export function err<TError>(error: TError): Error<TError> {
	return { success: false, error };
}
