import type { Session } from "next-auth";

import { err, ok } from "./result";
import { backendAuthorizationHeader } from "./url-helper";

async function decodeJsonResponse<TError>(response: Response) {
	if (!response.ok) {
		try {
			const errorDetails = await response.json();
			return err({
				code: "validation-error" as const,
				details: errorDetails as TError,
			});
		} catch {
			return err({
				code: "http-error" as const,
				details: response.status,
			});
		}
	}

	const result = await response.json();
	return ok(result);
}

export function createAnonymPostFetcher<TError>(url: string) {
	return async (data: { [key: string]: unknown }) => {
		const response = await fetch(url, {
			method: "POST",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		});

		return decodeJsonResponse<TError>(response);
	};
}

export function createAuthenticatedMutationFetcher<TError>(
	session: Session,
	method: "POST" | "PUT",
) {
	return async (url: string, data: { [key: string]: unknown }) => {
		const params = {
			method,
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
				...backendAuthorizationHeader(session.accessToken as string),
			},
		};

		const response = await fetch(url, params);

		return decodeJsonResponse<TError>(response);
	};
}

export function createAuthenticatedFetcher(
	session: Session,
	init?: RequestInit,
) {
	return async (url: string) => {
		const params: RequestInit = init || {};
		const authHeader = backendAuthorizationHeader(
			session.accessToken as string,
		);
		params.headers = {
			...params.headers,
			...authHeader,
		};

		const response = await fetch(url, params);
		return response.json();
	};
}
