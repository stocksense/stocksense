import { Stocks } from "@/components/stocks/Stocks";

export default function stocksPage() {
	return (
		<>
			<Stocks />
		</>
	);
}

stocksPage.auth = true;
