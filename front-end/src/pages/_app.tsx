import "@/styles.css";
import "@/polyfills";

import type { AppProps } from "next/app";
import Head from "next/head";
import { Provider } from "next-auth/client";

import WithAuth from "@/components/auth/WithAuth";
import { Layout } from "@/components/Layout";

export default function MyApp({
	Component,
	pageProps: { session, ...pageProps },
}: AppProps) {
	return (
		<Provider session={session}>
			<div>
				<Head>
					<title>StockSense</title>
					<link rel="icon" href="/favicon.ico" />
					<meta
						name="color-scheme"
						content="normal" // TODO: Set to "dark" or "light" based on theme
					/>
				</Head>
				<Layout>
					{(Component as any).auth ? (
						<WithAuth>
							<Component {...pageProps} />
						</WithAuth>
					) : (
						<Component {...pageProps} />
					)}
				</Layout>
			</div>
		</Provider>
	);
}
