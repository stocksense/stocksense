import { signOut, useSession } from "next-auth/client";

import { SignIn } from "@/components/auth/SignIn";

const LoggedIn = () => {
	return (
		<>
			<button type="button" onClick={() => signOut()}>
				Sign out
			</button>
		</>
	);
};

export default function Login() {
	const [session, loading] = useSession();

	if (loading) {
		return <>Loading...</>;
	}

	if (session) {
		return <LoggedIn />;
	}

	return (
		<>
			<div className="flex py-8 px-2 sm:px-6 lg:px-8 mx-auto max-w-7xl text-other-700">
				<section className="py-16 px-4 sm:px-6 lg:px-4 mx-auto max-w-6xl">
					<SignIn />
				</section>
			</div>
		</>
	);
}
