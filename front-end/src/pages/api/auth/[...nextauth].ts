import jwt from "jsonwebtoken";
import type { NextApiRequest, NextApiResponse } from "next";
import type { NextAuthOptions, User } from "next-auth";
import NextAuth from "next-auth";
import type { JWT } from "next-auth/jwt";
import Providers from "next-auth/providers";

import { makeInternalBackendUrl } from "@/helpers/url-helper";

interface Credentials {
	email: string;
	password: string;
}

interface AuthenticatedUser extends User {
	accessToken?: string;
	refreshToken?: string;
}

interface AccessTokenDetails {
	accessToken: string;
	accessTokenExpires: number;
}

type TokenDetails = AccessTokenDetails & {
	refreshToken: string;
};

function defaultTokenDetails(): TokenDetails {
	return {
		accessToken: "",
		accessTokenExpires: 0,
		refreshToken: "",
	};
}

function tokenDetailsToJWT(input: TokenDetails): JWT {
	return {
		accessToken: input.accessToken,
		accessTokenExpires: input.accessTokenExpires,
		refreshToken: input.refreshToken,
	};
}

function jwtToTokenDetails(input: JWT): TokenDetails {
	return {
		accessToken: input.accessToken as string,
		accessTokenExpires: input.accessTokenExpires as number,
		refreshToken: input.refreshToken as string,
	};
}

type AuthenticatedJWT = JWT & TokenDetails;

const backendAuthProvider = Providers.Credentials({
	name: "Stocksense",
	credentials: {
		email: { label: "Email", type: "email", placeholder: "Email address" },
		password: { label: "Password", type: "password" },
	},
	async authorize(credentials: Credentials) {
		const url = makeInternalBackendUrl("auth", "login");

		const body = JSON.stringify({
			email: credentials.email,
			password: credentials.password,
		});

		const response = await fetch(url, {
			method: "POST",
			body,
			headers: {
				"Content-Type": "application/json",
			},
		});

		if (response.ok) {
			const tokenDetails = await response.json();

			const user: AuthenticatedUser = {
				accessToken: tokenDetails.access,
				refreshToken: tokenDetails.refresh,
			};

			return user;
		}

		return null;
	},
});

function accessTokenExpiration(accessToken: string) {
	const decoded = jwt.decode(accessToken, { json: true });
	if (!decoded) {
		return false;
	}
	const expiration = decoded.exp || 0;

	return expiration * 1000;
}

async function refreshAccessToken(
	refreshToken: string,
): Promise<TokenDetails | false> {
	const url = makeInternalBackendUrl("auth", "login", "refresh");

	const body = JSON.stringify({
		refresh: refreshToken,
	});

	const response = await fetch(url, {
		method: "POST",
		body,
		headers: {
			"Content-Type": "application/json",
		},
	});

	if (!response.ok) {
		return false;
	}

	const content = await response.json();
	const accessToken = content.access;
	if (!accessToken) {
		return false;
	}

	const expiration = accessTokenExpiration(accessToken);
	if (expiration === false) {
		return false;
	}

	const newRefreshToken = content.refresh || refreshToken;

	return {
		accessToken,
		accessTokenExpires: expiration,
		refreshToken: newRefreshToken,
	};
}

const settings: NextAuthOptions = {
	providers: [backendAuthProvider],
	pages: {
		signIn: "/login",
	},
	callbacks: {
		signIn: async (user: AuthenticatedUser, account, metadata) => {
			if (account.type === "credentials") {
				user.email = metadata.email;

				return true;
			}

			return false;
		},
		jwt: async (token: JWT, user: AuthenticatedUser, account): Promise<JWT> => {
			if (user && account) {
				// initial signin
				const { accessToken, refreshToken } = user;

				if (!accessToken || !refreshToken) {
					return tokenDetailsToJWT(defaultTokenDetails());
				}

				// decode expiration date
				const expiration = accessTokenExpiration(accessToken);
				if (expiration === false) {
					return tokenDetailsToJWT(defaultTokenDetails());
				}

				const newToken: AuthenticatedJWT = {
					accessToken,
					accessTokenExpires: expiration,
					refreshToken,
				};
				return newToken;
			}

			const currentToken = jwtToTokenDetails(token);
			if (Date.now() < currentToken.accessTokenExpires) {
				// return the previous token if the access token has not expired yet
				return token;
			}

			// refresh the token
			const newToken = await refreshAccessToken(currentToken.refreshToken);
			if (newToken !== false) {
				return tokenDetailsToJWT(newToken);
			}
			return tokenDetailsToJWT(defaultTokenDetails());
		},
		session: async (session, user: AuthenticatedUser) => {
			if (!user || !user.accessToken) {
				return {};
			}

			// the the access token can be used on client side
			session.accessToken = user.accessToken;

			return session;
		},
	},
};

export default (req: NextApiRequest, res: NextApiResponse) => {
	return NextAuth(req, res, settings);
};

// https://next-auth.js.org/tutorials/refresh-token-rotation
// https://arunoda.me/blog/add-auth-support-to-a-next-js-app-with-a-custom-backend
// https://javascript.tutorialink.com/nextjs-authentication-with-next-auth-against-drf/
// https://mahieyin-rahmun.medium.com/how-to-configure-social-authentication-in-a-next-js-next-auth-django-rest-framework-application-cb4c82be137
// https://jeff.roche.cool/posts/django-next-auth/
