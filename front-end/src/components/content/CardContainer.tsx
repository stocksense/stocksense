import Image from "next/image";
import React from "react";

type CardProps = {
	title: string;
	footer: string;
	src: string;
};

const Card = ({ title, footer, src }: CardProps) => {
	return (
		<div className="flex flex-col justify-center w-full bg-secondary-700 rounded-lg shadow-lg transition duration-500 hover:scale-105">
			<div className="h-full mb-8">
				<Image
					className="object-cover rounded-t-lg"
					src={src}
					alt={`img-${title}`}
					width={600}
					height={400}
				/>
			</div>
			<div className="flex flex-col py-5 px-3 mx-auto space-y-5 w-2/3 h-auto rounded-3xl ">
				<p className="text-xl font-bold text-white ">{title}</p>
				<a role="button" className="text-base font-normal text-primary-400">
					{footer}
				</a>
			</div>
		</div>
	);
};

export const CardContainer = () => {
	return (
		<div className="w-full bg-other-600 drop-shadow-lg">
			<section className="py-16 px-4 sm:px-6 lg:px-4 mx-auto max-w-6xl">
				<div className="pb-12 text-center">
					<h1 className="text-3xl md:text-4xl lg:text-5xl font-bold text-other-200 font-heading ">
						Be there the next time a stock goes to the moon
					</h1>
				</div>
				<div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 gap-6 px-6">
					<Card
						title="Real time graphing and alerting system "
						footer="Read more"
						src="/static/images/card_1.jpg"
					/>
					<Card
						title="Sentiment analysis using social media"
						footer="Read more"
						src="/static/images/card_2.jpg"
					/>
					<Card
						title="State-of-the-art AI technology"
						footer="Read more"
						src="/static/images/card_3.jpg"
					/>
				</div>
			</section>
		</div>
	);
};
