import ParentSize from "@visx/responsive/lib/components/ParentSize";
import Link from "next/link";
import type { Session } from "next-auth";
import { useSession } from "next-auth/client";
import React, { useMemo, useState } from "react";
import { AiFillStar, AiOutlineStar } from "react-icons/ai";
import { FaArrowRight } from "react-icons/fa";
import useSWR from "swr";
import { query } from "urlcat";

import {
	createAuthenticatedFetcher,
	createAuthenticatedMutationFetcher,
} from "@/helpers/backend-fetcher";
import { classNames } from "@/helpers/classNames";
import { makeBackendUrl } from "@/helpers/url-helper";
import useDebounce from "@/helpers/use-debounce";

import { StockLineChart } from "./common/StockLineChart";
import { addChangeDir } from "./common/types/Calculator";
import StockFilter, { defaultFilter, FilterModel } from "./StockFilter";

export type Stock = {
	id: number;
	is_starred: boolean;
	eligibility: number;
	name: string;
	price: number;
	price_timestamp: Date;
	sentiment: number;
	sentiment_timestamp: Date;
	symbol: string;
};

const StocksTableHead = () => {
	return (
		<thead
			className="
			z-40 sticky top-0 bg-secondary-900 rounded-2xl border-b-2
			border-secondary-500 text-other-200 uppercase text-xs
			 "
		>
			<tr>
				<th className="py-3 pl-3 text-left">Favourite</th>
				<th className="py-3 text-left">Symbol</th>
				<th className="py-3 text-left">Name</th>
				<th className="py-3 text-left">Price</th>
				<th className="py-3 text-left">Sentiment</th>
				<th className="py-3 text-left">Price chart</th>
				<th className="py-3 text-center">Details</th>
			</tr>
		</thead>
	);
};

type StockTableRowProps = {
	stock: Stock;
	onStar: (starred: boolean) => void;
};

const StockTableRow = ({ stock, onStar }: StockTableRowProps) => {
	const [favourite, setFavourite] = useState(stock.is_starred);

	useMemo(() => {
		setFavourite(stock.is_starred);
	}, [stock.is_starred]);

	return (
		<tr className="hover:bg-secondary-800 border-b border-secondary-600 bg-gray-secondary-900">
			<td className="py-5 pl-5 text-left">
				<button
					type="button"
					className="text-2xl"
					onClick={() => {
						setFavourite(!favourite);
						onStar(!favourite);
					}}
				>
					{favourite ? <AiFillStar /> : <AiOutlineStar />}
				</button>
			</td>
			<td className="text-sm">
				<span className="inline-block relative py-1 px-2 font-semibold leading-tight">
					<span
						aria-hidden
						className="absolute inset-0 bg-green-200 rounded-full"
					/>
					<span className="relative text-other-900">{stock.symbol}</span>
				</span>
			</td>
			<td className="pr-2 text-sm">
				<p className="text-other-400 ">{stock.name}</p>
			</td>
			<td className="pr-2">
				<p className="font-bold text-white">${stock.price}</p>
			</td>
			<td className="pl-2 text-sm">
				<p
					className={classNames(
						stock.sentiment >= 0 ? "text-primary-500" : "text-error-500",
						"",
					)}
				>
					{addChangeDir(stock.sentiment)}
				</p>
			</td>
			<td className="text-sm ">
				<div className="flex items-center h-12 ml-3">
					<ParentSize>
						{({ width, height }) => (
							<StockLineChart id={stock.id} width={width} height={height} />
						)}
					</ParentSize>
				</div>
			</td>
			<td className="flex justify-center items-end pt-8">
				<Link href="/stocks/[symbol]" as={`/stocks/${stock.symbol}`}>
					<a>
						<FaArrowRight />
					</a>
				</Link>
			</td>
		</tr>
	);
};

type TableProps = {
	session: Session;
	filter: FilterModel;
};

const StockTableBody = ({ session, filter }: TableProps) => {
	const fetcher = createAuthenticatedFetcher(session);

	let url = makeBackendUrl("dashboard", "stocks");
	let params = {};
	if (filter.filter) {
		params = { ...params, filter: filter.filter };
	}
	if (filter.order !== "name") {
		params = { ...params, order: filter.order };
	}
	const queryString = query(params);
	if (queryString) {
		url += `?${queryString}`;
	}
	const { data, error } = useSWR(url, fetcher);

	async function handleStarClick(id: number, starred: boolean) {
		const starUrl = makeBackendUrl(
			"trader",
			"stocks",
			id.toString(),
			"starred",
		);
		const starFetcher = createAuthenticatedMutationFetcher(session, "PUT");
		await starFetcher(starUrl, {
			starred,
		});
	}

	if (error)
		return (
			<tbody>
				<tr>
					<td>failed to load data;</td>
				</tr>
			</tbody>
		);
	if (!data)
		return (
			<tbody>
				<tr>
					<td>
						<svg
							width="38"
							height="38"
							viewBox="0 0 38 38"
							xmlns="http://www.w3.org/2000/svg"
							stroke="#edf1f3"
						>
							<g fill="none" fillRule="evenodd">
								<g transform="translate(1 1)" strokeWidth="2">
									<circle strokeOpacity=".5" cx="18" cy="18" r="18" />
									<path d="M36 18c0-9.94-8.06-18-18-18">
										<animateTransform
											attributeName="transform"
											type="rotate"
											from="0 18 18"
											to="360 18 18"
											dur="1s"
											repeatCount="indefinite"
										/>
									</path>
								</g>
							</g>
						</svg>
					</td>
				</tr>
			</tbody>
		);

	return (
		<tbody>
			{data.map((stock: Stock) => {
				return (
					<StockTableRow
						key={`Stock-${stock.symbol}`}
						stock={stock}
						onStar={(starred) => handleStarClick(stock.id, starred)}
					/>
				);
			})}
		</tbody>
	);
};

export const Stocks = () => {
	const [session, loading] = useSession();
	const [currentFilter, debounceFilter] = useDebounce<FilterModel>(
		200,
		defaultFilter(),
	);

	if (loading || !session) {
		// waiting to load
		return null;
	}

	function handleFilterChange(value: FilterModel) {
		debounceFilter(value);
	}

	return (
		<div className="flex py-8 px-2 sm:px-6 lg:px-8 mx-auto max-w-7xl text-other-700">
			<section className="py-16 px-4 sm:px-6 lg:px-4 mx-auto max-w-6xl">
				<div className="flex pb-12">
					<div>
						<h1 className="text-4xl font-bold text-other-200 ">Dashboard</h1>
						<StockFilter onChange={(x) => handleFilterChange(x)} />
					</div>
				</div>
				<div className="flex flex-col h-screen">
					<div className="overflow-x-auto flex-grow bg-secondary-900 rounded-2xl shadow-lg no-scrollbar ">
						<table className="w-full border-collapse table-fixed ">
							<StocksTableHead />
							<StockTableBody session={session} filter={currentFilter} />
						</table>
					</div>
				</div>
			</section>
		</div>
	);
};
