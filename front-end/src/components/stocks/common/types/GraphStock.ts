import { bisector } from "d3-array";

export interface GraphStock {
	timestamp: Date;
	price: number;
}
export interface GraphSentiment {
	timestamp: Date;
	sentiment: number;
}

// accessors
export const getDate = (d: GraphStock | GraphSentiment) =>
	new Date(d.timestamp);

export const getStockValue = (d: GraphStock) => d.price;
export const getSentimentValue = (d: GraphSentiment) => d.sentiment;

export const bisectDate = bisector<GraphStock | GraphSentiment, Date>(
	(d) => new Date(d.timestamp),
).left;

export declare const graphStock: GraphStock[];
export declare const gaphSentiment: GraphSentiment[];
