export const ChangePercent = (from: number, to: number) => {
	return (((to - from) / Math.abs(to)) * 100).toFixed(4);
};

export const ChangeTotal = (from: number, to: number) => {
	return to - from;
};

export const addChangeDir = (change: number) => {
	if (change === 0) return "-";
	return (change < 0 ? "▼ " : "▲ ") + change.toFixed(4);
};
