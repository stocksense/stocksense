import { ParentSize } from "@visx/responsive";
import { max, min } from "d3-array";
import { useRouter } from "next/router";
import React from "react";
import useSWR from "swr";

import { classNames } from "@/helpers/classNames";
import { fetcher } from "@/helpers/fetcher";
import { makeBackendUrl } from "@/helpers/url-helper";

import AreaChart from "./common/AreaChart";
import {
	addChangeDir,
	ChangePercent,
	ChangeTotal,
} from "./common/types/Calculator";
import { getStockValue } from "./common/types/GraphStock";
import { StockPosts } from "./StockPosts";

export const StockDetails = () => {
	const router = useRouter();
	const { symbol } = router.query;

	const { data: stock, error } = useSWR(
		makeBackendUrl("dashboard", "stocks", symbol as string),
		fetcher,
	);

	const { data: stockGraph } = useSWR(
		() => `${makeBackendUrl("dashboard", "stocks")}/${stock.id}/graph`,
		fetcher,
	);

	if (error) return <div>Error</div>;
	if (!stockGraph) {
		return <div>Loading...</div>;
	}

	return (
		<div className="flex py-8 px-2 sm:px-6 lg:px-8 mx-auto max-w-7xl text-other-700">
			<div className="flex-grow py-6 px-4">
				<div className="flex justify-between pb-12">
					<div>
						<h1 className="text-4xl font-bold text-other-200 ">Details</h1>
					</div>
				</div>
				<div className="flex flex-col bg-secondary-900 rounded-2xl shadow-lg ">
					<div className="flex flex-col p-10 w-full h-screen">
						<div className="w-full h-1/4">
							<h3 className="text-lg font-semibold leading-tight ">
								{stock.name}
							</h3>
							<h6 className="py-1 text-sm leading-tight mb-2">
								<span>{stock.symbol}</span>
								&nbsp;&nbsp;-&nbsp;&nbsp;{" "}
								<span>{new Date(stock.price_timestamp).toLocaleString()}</span>
							</h6>
							<div className="flex items-end py-3 w-full mb-6">
								<span className="block text-3xl leading-none">
									${stock.price}
								</span>
								<span
									className={classNames(
										ChangeTotal(
											getStockValue(stockGraph.prices.at(0)),
											getStockValue(stockGraph.prices.at(-1)),
										) > 0
											? "text-primary-500"
											: "text-error-500",
										"block pl-5 text-sm leading-5  ml-4",
									)}
								>
									{`${addChangeDir(
										ChangeTotal(
											getStockValue(stockGraph.prices.at(0)),
											getStockValue(stockGraph.prices.at(-1)),
										),
									)} (${ChangePercent(
										getStockValue(stockGraph.prices.at(0)),
										getStockValue(stockGraph.prices.at(-1)),
									)}%)`}
								</span>
							</div>
							<div className="flex w-full text-xs">
								<div className="flex w-5/12">
									<div className="flex-1 pr-3 font-semibold text-left">
										High
									</div>
									<div className="px-3 text-right">
										{max(stockGraph.prices, getStockValue) || 0}
									</div>
								</div>
							</div>
							<div className="flex w-full text-xs">
								<div className="flex w-5/12">
									<div className="flex-1 pr-3 font-semibold text-left">Low</div>
									<div className="px-3 text-right">
										{min(stockGraph.prices, getStockValue) || 0}
									</div>
								</div>
							</div>
						</div>
						<div className="w-full h-2/3">
							<ParentSize>
								{({ width, height }) => (
									<AreaChart
										width={width}
										height={height}
										prices={stockGraph.prices}
										sentiments={stockGraph.sentiments}
									/>
								)}
							</ParentSize>
						</div>
					</div>
					<div>
						<StockPosts symbol={symbol ? (symbol as string) : ""} />
					</div>
				</div>
			</div>
		</div>
	);
};
