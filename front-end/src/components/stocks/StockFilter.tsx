import * as React from "react";
import { Controller, useForm } from "react-hook-form";

import { Input } from "@/components/inputs/Input";

export type FilterModel = {
	filter: string;
	order: "name" | "starred" | "promising";
};

export function defaultFilter(): FilterModel {
	return { filter: "", order: "name" };
}

type Props = {
	onChange: (model: FilterModel) => void;
};

export default function StockFilter({ onChange }: Props) {
	const { register, watch, control, getValues } = useForm<FilterModel>();

	React.useEffect(() => {
		const subscription = watch(() => {
			onChange(getValues());
		});
		return () => subscription.unsubscribe();
	}, [watch, onChange, getValues]);

	return (
		<>
			<form>
				<div className="flex space-y-3">
					<div>
						<Controller
							name="filter"
							control={control}
							render={({ field }) => (
								<Input type="text" placeholder="Search" {...field} />
							)}
						/>
					</div>
					<div className="flex px-4">
						Order:
						<select {...register("order", { required: true })}>
							<option value="name">by name</option>
							<option value="promising">by promising</option>
							<option value="starred">by starred</option>
						</select>
					</div>
				</div>
			</form>
		</>
	);
}
