import React from "react";

export const Footer = () => {
	return (
		<footer className="px-2 sm:px-6 lg:px-8 pt-8 pb-9 mx-auto max-w-7xl ">
			<div className="container mx-auto">
				<div className="flex flex-wrap pb-4 text-left lg:text-left">
					<div className="px-4 w-full lg:w-6/12">
						<h4 className="text-3xl text-other-200 fonat-semibold">
							Let&apos;s keep in touch!
						</h4>
						<h5 className="text-other-600 text-md mt-0 mb-2">
							Find us on any of these platforms, we respond 1-2 business days.
						</h5>
					</div>
					<div className="px-4 w-full lg:w-6/12">
						<div className="flex flex-wrap items-top">
							<div className="px-4 ml-auto w-full lg:w-4/12">
								<span className="block py-2 text-sm font-semibold text-other-500 uppercase mb-2">
									Other Resources
								</span>
								<ul className="list-unstyled">
									<li>
										<a
											className="block pb-2 text-sm text-primary-500 hover:text-other-200"
											href=""
										>
											Terms &amp; Conditions
										</a>
									</li>
									<li>
										<a
											className="block pb-2 text-sm text-primary-500 hover:text-other-200"
											href=""
										>
											Privacy Policy
										</a>
									</li>
									<li>
										<a
											className="block pb-2 text-sm text-primary-500 hover:text-other-200"
											href=""
										>
											Contact Us
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<hr className="pt-3 border-other-200 my-6" />
				<div className="flex flex-wrap justify-center md:justify-between items-center">
					<div className="px-4 mx-auto w-full md:w-4/12 text-center">
						<div className="py-3 text-sm text-other-200">
							Copyright <span id="get-current-year">2021</span> StockSense.
						</div>
					</div>
				</div>
			</div>
		</footer>
	);
};
