import React from "react";

export const Hero = () => {
	return (
		<section className="mx-auto max-w-6xl text-other-500">
			<div className="pt-16 sm:pt-24 lg:pt-40 pb-80 sm:pb-40 lg:pb-48 ">
				<div className="px-4 sm:px-6 lg:px-8 mx-auto">
					<div className="sm:max-w-2xl">
						<h1 className="text-4xl sm:text-6xl font-extrabold tracking-tight text-other-200 ">
							Real time sentiment analysis
						</h1>
						<p className="text-xl text-gray-500 mt-4">
							For all your trading needs
						</p>
					</div>
				</div>
			</div>
		</section>
	);
};
