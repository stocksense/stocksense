const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
	mode: "jit",
	purge: ["./src/**/*.{js,ts,tsx}"],
	darkMode: "class",
	theme: {
		extend: {
			colors: {
				primary: {
					100: "#d9f8e5",
					200: "#b3f1cc",
					300: "#8cebb2",
					400: "#66e499",
					500: "#40dd7f",
					600: "#33b166",
					700: "#26854c",
					800: "#1a5833",
					900: "#0d2c19",
				},
				secondary: {
					100: "#d2d9db",
					200: "#a5b2b6",
					300: "#798c92",
					400: "#4c656d",
					500: "#1f3f49",
					600: "#19323a",
					700: "#13262c",
					800: "#0c191d",
					900: "#060d0f",
				},
				other: {
					100: "#f6f8f9",
					200: "#edf1f3",
					300: "#e4ebee",
					400: "#dbe4e8",
					500: "#d2dde2",
					600: "#a8b1b5",
					700: "#7e8588",
					800: "#54585a",
					900: "#2a2c2d",
				},
				error: {
					100: "#F8C9D7",
					200: "#F4A4BC",
					300: "#F080A2",
					400: "#EB5B87",
					500: "#E7376C",
					600: "#CD194F",
					700: "#9B133C",
					800: "#690D28",
					900: "#370715",
				},
			},

			fontFamily: { sans: ["Roboto", "Helvetica", "Arial", "sans-serif"] },

			transitionTimingFunction: {
				DEFAULT: defaultTheme.transitionTimingFunction.out,
			},
		},
		margin: (theme, { negative }) => ({
			auto: "auto",
			...negative(theme("spacing")),
		}),
	},
	corePlugins: {
		animation: false,
	},
};
